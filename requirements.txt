aiosql==3.2.0
alembic==1.6.5
asgi-lifespan==1.0.1
asyncpg==0.23.0
docker==5.0.0
docker-compose==1.29.2
docker-pycreds==0.4.0
dockerpty==0.4.1
pytest==6.2.4
pytest-cov==2.12.1
fastapi==0.66.0
httpx==0.18.2
PyPika==0.48.7
passlib==1.7.4
jwt==1.2.0




